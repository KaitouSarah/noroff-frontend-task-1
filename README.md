# Noroff Frontend Task 1

Note: Task 1 and 2 are in root, while 3, 4 and 5 are collected in webpack-starter folder in a single html.
__________________________

Task 1 - Math stuff

 Write a small program to display the first 50 numbers in
the fibonacci sequence

 Here is how it works:

 https://www.mathsisfun.com/numbers/fibonaccisequence.html

 TIPS: Use a while or for loop

__________________________

Task 2 - FizzBuzz

 Write a small program to display the words Fizz and Buzz

 Write a loop up to 50

 If the current loop index is divisible by 3 - Display Fizz

 If the current loop index is divisible by 5 - Display Buzz

 If the current loop index is divisible by 15 - Display
FizzBuzz

Note from developer: This description implies that the solution should print both "Fizz" and "FizzBuzz" if the index is divisible by 15.
__________________________

Task 3 - JavaScript Clock

 Build a JavaScript Clock

 Display the current time and date on a webpage

 Use moment js

 Install it with npm

 Use Webpack starter (Includes Moment)

https://github.com/sumodevelopment/webpack-starter.git

 https://momentjs.com/

 Be creative add an image 

__________________________

Task 4 - Build a calculator

 Build a calculator using JavaScript and HTML

 Use buttons for numbers and display the results on a
web page

 The user should be able to add, subtract, divide and
multiply.

 Check for rounding

 Make it pretty-ish

__________________________

Task 3 - Build a Rock/Paper/Scissors Game

 Build a rock papers scissors game

 The user should choose their hand

 The CPU Player should the respond with their hand

 Rock Beats Scissor

 Scissor Beats Paper

 Paper Beats Rock
