

for (var i = 0; i < 50; i++) {
    console.log(`i = ${i}: `);
  if (i % 3 === 0) {
      console.log('Fizz');
  }
  if (i % 5 === 0) {
      console.log('Buzz');
  }
  if (i % 15 === 0) {
      console.log('FizzBuzz');
  }
  console.log();
}

/*
    I think there might be an error in the task description. 
    At this point it tells me to display FIzz if the iterator
    is divisible by 3, and Buzz if it is divisible by 5, and 
    FizzBuzz if it is divisible by 15. I does not say anything
    about leaving out Fizz or Buzz if it is divisible by 15. 
*/